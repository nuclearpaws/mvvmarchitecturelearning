﻿using System.Windows.Controls;

namespace MvvmArchitecture.Views
{
    public partial class ReusableView : UserControl
    {
        public ReusableView()
        {
            InitializeComponent();
        }
    }
}
