﻿namespace MvvmArchitecture.Models
{
    public class ReusableModel
    {
        public string MyText { get; set; }

        public ReusableModel(string myText = default(string))
        {
            MyText = myText;
        }
    }
}
