﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using MvvmArchitecture.Models;
using MvvmArchitecture.Utility;

namespace MvvmArchitecture.ViewModels
{
    public class ReusableViewModel : INotifyPropertyChanged
    {
        private ReusableModel _reusableModel;

        public ReusableModel ReusableModel
        {
            get => _reusableModel;
            set
            {
                _reusableModel = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(MyText));
            }
        }

        public string MyText
        {
            get => _reusableModel.MyText;
            set
            {
                _reusableModel.MyText = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand MyCommand => new RelayCommand(
            o => MessageBox.Show(MyText),
            o => !string.IsNullOrWhiteSpace(MyText));

        public ReusableViewModel(ReusableModel reusableModel)
        {
            _reusableModel = reusableModel;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
