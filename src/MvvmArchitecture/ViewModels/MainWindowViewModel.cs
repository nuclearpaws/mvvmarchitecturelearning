﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using MvvmArchitecture.Models;
using MvvmArchitecture.Utility;

namespace MvvmArchitecture.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<ReusableModel> _reusableModels;
        private ReusableViewModel _selectedItem;

        public ObservableCollection<ReusableModel> ReusableModels
        {
            get => _reusableModels;
            set
            {
                _reusableModels = value;
                ReusableModels.CollectionChanged += (sender, e) =>
                {
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(ReusableViewModels));
                    //OnPropertyChanged(nameof(RemoveCommand));
                };
            }
        }

        public ObservableCollection<ReusableViewModel> ReusableViewModels =>
            new ObservableCollection<ReusableViewModel>(
                ReusableModels.Select(m => new ReusableViewModel(m)));

        public ReusableViewModel SelectedItem
        {
            private get => _selectedItem;
            set
            {
                _selectedItem = value;
                OnPropertyChanged();
                //OnPropertyChanged(nameof(RemoveCommand));
            }
        }

        public RelayCommand AddCommand => new RelayCommand(
            o => ReusableModels.Add(new ReusableModel()),
            o => true);

        public RelayCommand RemoveCommand => new RelayCommand(
            o => ReusableModels.Remove(SelectedItem.ReusableModel),
            o => SelectedItem != null);

        public MainWindowViewModel()
        {
            ReusableModels = new ObservableCollection<ReusableModel>
            {
                new ReusableModel("Test Item 1"),
                new ReusableModel("Test Item 2")
            };
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
